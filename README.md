# API Usage

## Bitcoin Network

By default all urls are `mainnet`. To use testnet add `?network=testnet` to the query of the url

## Encryption

On chain encryption is available upon request. Message [@1](https://twetch.app/u/1) for access

## Upload

To use Dogefiles, you first upload a file, then you pay for it to be settled on the bitcoin ledger.

Request

```
POST https://ink.twetch.com/dogefiles/upload
Content-Type: multipart/form-data
Body:
  file: <file-contents>
```

Response

```javascript
{
    payment_address: '18Qi1rXJSLDLUYDZVkRT3ZdyB3E9eZamY2',
    payment_sats_needed: 2833,
    file_url: '<file-url>',
    file_size: '<file-size>',
    file_content_type: '<mime-type>'
}
```


## Pay

To pay for a file, send `payment_sats_needed` to `payment_address`. You can pay for multiple files at once in the same transaction and the response will return an array of the files you uploaded

Request

```
POST https://ink.twetch.com/dogefiles/pay
Content-Type: application/octet-stream
Body: <rawtx>
```

Response

```javascript
['f7a3e5838a134a78b6a5033aa928efb7849be6212307b9b9eed3c738ea470bc2']
```

## Download

You can download a file by using it's transaction id. This link can be used in html tags like `<img>`, `<video>` or `<audio>`

Request

```
GET https://ink.twetch.com/dogefiles/[txid]
```


# Authors

- [@1](https://twetch.app/u/1)
